package com.pn.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBHelper {
	
	final static String URL = "jdbc:mysql://localhost:3306/db2_practice_student";
	final static String DRIVER = "com.mysql.jdbc.Driver";
	final static String USER = "root";
	final static String PASSWORD = "";
	
	public DBHelper(){
		try {
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static Connection returnConnection(){
		DBHelper db = new DBHelper();
		try {
			Connection cn = DriverManager.getConnection(URL,USER,PASSWORD);
			return cn;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null ;	
	}
}

package com.pn.controller;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;


@MultipartConfig(maxFileSize = 1699999999)
/**
 * Servlet implementation class ChangeProfileController
 */
@WebServlet("/ChangeProfileController")
public class ChangeProfileController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ChangeProfileController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		getServletContext().getRequestDispatcher("/changeprofile.html").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		 //getServletContext().getRequestDispatcher("/login_form.html").forward(request, response);
		try {
            Part loadfile = request.getPart("avarta");
            InputStream inputStream = null;
            if (loadfile == null) {
                long loadsize = loadfile.getSize();
                String filecontent = loadfile.getContentType();
                inputStream = loadfile.getInputStream();
            }
            Connection cn;
            ResultSet rs = null;
            PreparedStatement stmt = null;
            try {

                Class.forName("com.mysql.jdbc.Driver");

                cn = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdb_exam?user=root&password=");
                if (cn != null) {

                    System.out.println("dang nhap thanh cong");

                }
                
                stmt = cn.prepareStatement("update jdb_exam set averta= ? where id = ?");
                
                stmt.setBlob(1, inputStream);
                stmt.setString(2, "1");
                 stmt.executeUpdate();
                 
            } catch (Exception ex) {
            	
            }

        } catch (Exception e) {
            System.out.println("severlet.load.doPost()");
        }

    }
		
	

}

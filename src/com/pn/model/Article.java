package com.pn.model;

import java.sql.Date;

public class Article {
	private int id;
	private int idUser;
	private String content;
	private Date dateCreate;
	private Date dateUpdate;
	
	public Article(int id, int idUser, String content, Date dateCreate, Date dateUpdate) {
		super();
		this.id = id;
		this.idUser = idUser;
		this.content = content;
		this.dateCreate = dateCreate;
		this.dateUpdate = dateUpdate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getDateCreate() {
		return dateCreate;
	}

	public void setDateCreate(Date dateCreate) {
		this.dateCreate = dateCreate;
	}

	public Date getDateUpdate() {
		return dateUpdate;
	}

	public void setDateUpdate(Date dateUpdate) {
		this.dateUpdate = dateUpdate;
	}
	
	
	
	
}

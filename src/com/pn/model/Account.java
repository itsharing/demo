package com.pn.model;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.PreparedStatement;

public class Account {
	
	private int id;
	
	private String email;
	private String password;
	private int isActive;
	private String verifyCode;
	private Date dateCreate;
	private Date dateUpdate;
	private Profile profile;
	
	public Account(int id, String email, String password, int isActive, String verifyCode,
			Date dateCreate, Date dateUpdate) {
		super();
		this.id = id;
		this.email = email;
		this.password = password;
		this.isActive = isActive;
		this.verifyCode = verifyCode;
		this.dateCreate = dateCreate;
		this.dateUpdate = dateUpdate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public String getVerifyCode() {
		return verifyCode;
	}

	public void setVerifyCode(String verifyCode) {
		this.verifyCode = verifyCode;
	}

	public Date getDateCreate() {
		return dateCreate;
	}

	public void setDateCreate(Date dateCreate) {
		this.dateCreate = dateCreate;
	}

	public Date getDateUpdate() {
		return dateUpdate;
	}

	public void setDateUpdate(Date dateUpdate) {
		this.dateUpdate = dateUpdate;
	}
	
	public void postArticle(){
		ArrayList<Article> articles = new ArrayList<Article>();
		
	}
	
	
	
}
